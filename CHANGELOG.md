# Changelog

## [24.1.0] - 30-01-2024

* Updated for release 24.1.0

## [20.10.0] - 26-10-2020

* Updated config files for new simplified pilot
* Updated config files for the new `cola2_safety` package

## [3.2.0] - 06-11-2019
* Add independent imu launchfile
* Delete node names from configurations
* Rename launchfiles to have common names between vehicles
* Applied cola2 lib refactor changes

## [3.1.0] - 25-02-2019

* First release
